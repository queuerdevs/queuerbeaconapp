var firebase = require("firebase");

var config = {
	apiKey: "AIzaSyDN7aKyP6tpaACzSlc8hyJWJwxiW6a_dyo",
	authDomain: "queyer-hive.firebaseapp.com",
	databaseURL: "https://queyer-hive.firebaseio.com",
	projectId: "queyer-hive",
	storageBucket: "queyer-hive.appspot.com",
	messagingSenderId: "993530086844",
};
firebase.initializeApp(config);

var database = firebase.database();

var bleno = require('bleno');
var os = require('os');
var util = require('util');

var service_key  = '1006';
var uuid = 'ff51b30e-d7e2-4d93-8842-a7c4a57dfb00';

var Descriptor = bleno.Descriptor;
var descriptor = new Descriptor({
    uuid: '2901',
    value: 'passKey' // static value, must be of type Buffer or string if set
});

var BlenoCharacteristic = bleno.Characteristic;

var ClientCharacteristic = function() {
  ClientCharacteristic.super_.call(this, {
    uuid: service_key,
    properties: ['read','write'],
//    secure: ['read','write'],
    descriptors: [descriptor],
  });

 this._value = new Buffer(0);
};

ClientCharacteristic.prototype.onReadRequest = function(offset, callback) {

  if(!offset) {

    this._value = new Buffer(JSON.stringify({
      'freeMemory' : os.freemem(),
      'totalMemory' : os.totalmem()
    }));
  }

    console.log('ClientCharacteristic - onReadRequest: value = ' +
      this._value.slice(offset, offset + bleno.mtu).toString()
    );

  callback(this.RESULT_SUCCESS, this._value.slice(offset, this._value.length));
};

function firebaseRead(path){
	var promise = new Promise(function(resolve, reject){
		var ref = firebase.database().ref('/users/' + uuid + path);
		
		ref.once('value').then(function(snapshot) {
			resolve(snapshot);
		}).catch((e)=>{
			console.log(e);
		});
	});
	return promise;
}

function firebaseWrite(path, data){
        var promise = new Promise(function(resolve, reject){
                var ref = firebase.database().ref('/qbeacons/' + uuid + path);
		
		var new_ref = ref.push();  			
		new_ref.set({
			key: data.key
		}).catch((response)=>{
			resolve(response);
		});
        });
        return promise;
}


ClientCharacteristic.prototype.onWriteRequest = function(received_data, offset, withoutResponse, callback) {

  	if(!offset) {
 		this._value = received_data.slice(offset, offset + bleno.mtu).toString();
		

 		firebase.auth().onAuthStateChanged(function(user) {
			if (user) {
			    console.log(user.key);

	            var ref = firebase.database().ref('/users/' + user.key + '/clients').push();			
				new_ref.set({
					client_key: this._value
				}).catch((response)=>{
					console.log(response);
				});
			} else {
		    // No user is signed in.
		  	}
		});



/*		firebaseRead('/clients').then((response)=>{
			//console.log(response.val());
		 
			var new_data = received_data.slice(offset, offset + bleno.mtu).toString();
			var exists = 0;

			response.forEach(function(client){
				var key = client.val().key;
				//console.log(new_data+' == '+key);	
				if(new_data==key){
					exists++;
				}
			});

			if(exists==0){
				data = {
					key: this._value
				};		

				firebaseWrite('/clients', data).then((response)=>{
					console.log(response);
				});	
			}else{
				console.log('Client '+new_data+' is already in line.');
			}
		});*/
//		firebase.database().ref('/qbeacons/' + uuid + '/clients/key').set({
//			key: this._value
//		});
	}



    console.log('ClientCharacteristic - onWriteReques: value = ' +
      this._value
    );

//console.log(BlenoCharacteristic);

  callback(this.RESULT_SUCCESS, this._value.slice(offset, this._value.length));
};

util.inherits(ClientCharacteristic, BlenoCharacteristic);
module.exports = ClientCharacteristic;
