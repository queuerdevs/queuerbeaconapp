var bleno = require('bleno');
var os = require('os');
var util = require('util');

var serial = 'a7c4a4li7b05';

var Descriptor = bleno.Descriptor;
var descriptor = new Descriptor({
    uuid: '2901',
    value: 'passKey' // static value, must be of type Buffer or string if set
});

var BlenoCharacteristic = bleno.Characteristic;

var PassKeyCharacteristic = function() {
  PassKeyCharacteristic.super_.call(this, {
    uuid: 'ff51b30e-d7e2-4d93-8842-'+serial,
    properties: ['read','write'],
//    secure: ['read','write'],
    descriptors: [descriptor],
  });

 this._value = new Buffer(0);
};

PassKeyCharacteristic.prototype.onReadRequest = function(offset, callback) {

  if(!offset) {

    this._value = new Buffer(JSON.stringify({
      'freeMemory' : os.freemem(),
      'totalMemory' : os.totalmem()
    }));
  }

    console.log('PassKeyCharacteristic - onReadRequest: value = ' +
      this._value.slice(offset, offset + bleno.mtu).toString()
    );

  callback(this.RESULT_SUCCESS, this._value.slice(offset, this._value.length));
};

PassKeyCharacteristic.prototype.onWriteRequest = function(data, offset, withoutResponse, callback) {

  if(!offset) {
 	this._value = data;

  }

    console.log('PassKeyCharacteristic - onWriteReques: value = ' +
      this._value.slice(offset, offset + bleno.mtu).toString()
    );

  callback(this.RESULT_SUCCESS, this._value.slice(offset, this._value.length));
};

util.inherits(PassKeyCharacteristic, BlenoCharacteristic);
module.exports = PassKeyCharacteristic;
