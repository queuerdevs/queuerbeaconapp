var bleno = require('bleno');
var util = require('util');

var LoadAverageCharacteristic = require('./characteristics/loadaverage');
var UptimeCharacteristic = require('./characteristics/uptime');
var MemoryCharacteristic = require('./characteristics/memory');
var PassKeyCharacteristic = require('./characteristics/passKey');
var ClientCharacteristic = require('./characteristics/client');

var Descriptor = bleno.Descriptor;

var descriptor = new Descriptor({
    uuid: 'ff51b30e-d7e2-4d93-8842-a7c4a57dfb00'
});

function SystemInformationService() {

  bleno.PrimaryService.call(this, {
    uuid: 'ff51b30e-d7e2-4d93-8842-a7c4a57dfb00',
    characteristics: [
	new LoadAverageCharacteristic(),
 	new UptimeCharacteristic(),
	new MemoryCharacteristic(),
     	new PassKeyCharacteristic(),
	new ClientCharacteristic()
    ]
  });
};

util.inherits(SystemInformationService, bleno.PrimaryService);
module.exports = SystemInformationService;
