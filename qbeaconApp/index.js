var bleno = require('bleno');

var SystemInformationService = require('./systeminformationservice');

var systemInformationService = new SystemInformationService();

bleno.name = "DS Riviera 1003B";
bleno.uuid = "ff51b30e-d7e2-4d93-8842-a7c4a57dfb00";

bleno.on('stateChange', function(state) {
  console.log('on -> stateChange: ' + state);

  if (state === 'poweredOn') {

    bleno.startAdvertising(bleno.name, [systemInformationService.uuid]);
  }
  else {

    bleno.stopAdvertising();
  }
});

bleno.on('advertisingStart', function(error) {

  console.log('on -> advertisingStart: ' +
    (error ? 'error ' + error : 'success')
  );

  if (!error) {

    bleno.setServices([
      systemInformationService
    ]);
  }
});
